package pubsub

import (
	"encoding/json"

	"github.com/ThreeDotsLabs/watermill/message"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/log"
	"gitlab.com/aerilyn/service-notification/internal/app/notification/usecase"
)

func EmailSuccessRegisterMessagingHandler(u usecase.EmailSuccessRegister, cfg config.Config) message.NoPublishHandlerFunc {
	return func(msg *message.Message) error {
		ctx := msg.Context()
		cmd := usecase.EmailSuccessRegisterCommand{}
		if err := json.Unmarshal(msg.Payload, &cmd); err != nil {
			log.WithCTX(ctx).Error(err)
			return err
		}

		err := u.EmailSuccessRegister(ctx, cmd)
		if err != nil {
			log.WithCTX(ctx).Error(err)
			return err
		}

		return nil
	}
}
