package pubsub

import (
	"github.com/ThreeDotsLabs/watermill/message"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-notification/internal/app/notification/usecase"
)

const (
	EmailSuccessRegisterHandlerName      = "EmailSuccessRegister"
	EmailSuccessRegisterHandlerTopicName = "email-successRegister"
)

type NotificationHandlerRegistryOptions struct {
	EmailSuccessRegister usecase.EmailSuccessRegister
	Subscriber           message.Subscriber
	Config               config.Config
}

type NotificationHandlerRegistry struct {
	options NotificationHandlerRegistryOptions
}

func NewNotificationHandlerRegistry(options NotificationHandlerRegistryOptions) *NotificationHandlerRegistry {
	return &NotificationHandlerRegistry{options: options}
}

func (m NotificationHandlerRegistry) RegisterHandlerToRouter(r *message.Router) error {
	r.AddNoPublisherHandler(
		EmailSuccessRegisterHandlerName,
		EmailSuccessRegisterHandlerTopicName,
		m.options.Subscriber,
		EmailSuccessRegisterMessagingHandler(m.options.EmailSuccessRegister, m.options.Config),
	)
	return nil
}
