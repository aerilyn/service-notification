package usecase

//go:generate mockgen -source=emailSuccessRegister.go -destination=usecasemock/emailSuccessRegister_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
)

type EmailSuccessRegisterCommand struct {
	Username string `json:"username"`
	Email    string `json:"email"`
}

type EmailSuccessRegister interface {
	EmailSuccessRegister(ctx context.Context, cmd EmailSuccessRegisterCommand) errors.CodedError
}
