package usecaseimpl

import (
	"context"
	"fmt"

	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/log"
	"gitlab.com/aerilyn/service-notification/internal/app/notification/usecase"
	"gitlab.com/aerilyn/service-notification/internal/pkg/email/gmail"
	pkgGmail "gitlab.com/aerilyn/service-notification/internal/pkg/email/gmail"
	"gopkg.in/gomail.v2"
)

type EmailSuccessRegisterOptions struct {
	Config      config.ConfigEnv
	GmailDialer pkgGmail.GomailDialer
}

type EmailSuccessRegister struct {
	options EmailSuccessRegisterOptions
}

func NewEmailSuccessRegister(options EmailSuccessRegisterOptions) *EmailSuccessRegister {
	return &EmailSuccessRegister{
		options: options,
	}
}

//send email success register to user who signup
func (s *EmailSuccessRegister) EmailSuccessRegister(ctx context.Context, cmd usecase.EmailSuccessRegisterCommand) errors.CodedError {
	mailer := gomail.NewMessage()
	mailer.SetHeader(gmail.FromKey, s.options.Config.Get(config.GMAIL_SENDER_NAME))
	mailer.SetHeader(gmail.ToKey, cmd.Email)
	mailer.SetHeader(gmail.SubjectKey, "Account Hobihub Anda Sudah Dibuat")
	mailer.SetBody(gmail.BodyHtml, fmt.Sprintf("Halo %s, terima kasih sudah mendaftar di hobihub", cmd.Username))
	err := s.options.GmailDialer.DialAndSend(mailer)
	if err != nil {
		err := errors.NewInternalSystemError().CopyWith(errors.Message(err.Error()))
		log.WithCTX(ctx).Error(err)
		return nil
	}
	log.WithCTX(ctx).Info(fmt.Sprintf("success send email success register to %s", cmd.Email))
	return nil
}
