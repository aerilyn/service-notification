package usecaseimpl

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/aerilyn/service-library/config"
	configMock "gitlab.com/aerilyn/service-library/config/mock"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-notification/internal/app/notification/usecase"
	pkgGmailMock "gitlab.com/aerilyn/service-notification/internal/pkg/email/gmail/mock"
)

func TestItemCreate(t *testing.T) {
	ctx := context.Background()
	emailSuccessRegisterCommand := usecase.EmailSuccessRegisterCommand{
		Username: "",
		Email:    "",
	}
	Convey("Testing Notification EmailSuccessRegister", t, func() {
		mockCtrl := gomock.NewController(t)
		Reset(func() {
			mockCtrl.Finish()
		})
		configMock := configMock.NewMockConfigEnv(mockCtrl)
		pkgGmailMock := pkgGmailMock.NewMockGomailDialer(mockCtrl)
		emailSuccessRegisterOptions := EmailSuccessRegisterOptions{
			Config:      configMock,
			GmailDialer: pkgGmailMock,
		}
		uc := NewEmailSuccessRegister(emailSuccessRegisterOptions)
		Convey("when DialSend return error should return error nil", func() {
			configMock.EXPECT().Get(config.GMAIL_SENDER_NAME).Return("")
			pkgGmailMock.EXPECT().DialAndSend(gomock.Any()).Return(errors.NewInternalSystemError())

			errCode := uc.EmailSuccessRegister(ctx, emailSuccessRegisterCommand)
			So(errCode, ShouldBeNil)
		})

		Convey("when all process not return error  should return error nil", func() {
			configMock.EXPECT().Get(config.GMAIL_SENDER_NAME).Return("")
			pkgGmailMock.EXPECT().DialAndSend(gomock.Any()).Return(nil)

			errCode := uc.EmailSuccessRegister(ctx, emailSuccessRegisterCommand)
			So(errCode, ShouldBeNil)
		})
	})
}
