package notification

import (
	"github.com/google/wire"
	"gitlab.com/aerilyn/service-notification/internal/app/notification/delivery/pubsub"
	"gitlab.com/aerilyn/service-notification/internal/app/notification/usecase"
	"gitlab.com/aerilyn/service-notification/internal/app/notification/usecase/usecaseimpl"
)

var ModuleSet = wire.NewSet(
	repositoryModuleSet,
	validatorModuleSet,
	usecaseModuleSet,
	deliveryModuleSet,
)

var repositoryModuleSet = wire.NewSet()

var deliveryModuleSet = wire.NewSet(
	wire.Struct(new(pubsub.NotificationHandlerRegistryOptions), "*"),
	pubsub.NewNotificationHandlerRegistry,
)

var validatorModuleSet = wire.NewSet()

var usecaseModuleSet = wire.NewSet(
	wire.Struct(new(usecaseimpl.EmailSuccessRegisterOptions), "*"),
	wire.Bind(new(usecase.EmailSuccessRegister), new(*usecaseimpl.EmailSuccessRegister)),
	usecaseimpl.NewEmailSuccessRegister,
)
