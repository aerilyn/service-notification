package healthcheck

import (
	"gitlab.com/aerilyn/service-notification/internal/app/healthcheck/delivery/web"

	"github.com/google/wire"
)

var ModuleSet = wire.NewSet(
	web.NewHandlerRegistry,
)
