package app

import (
	healthcheckweb "gitlab.com/aerilyn/service-notification/internal/app/healthcheck/delivery/web"
	notificationPubsub "gitlab.com/aerilyn/service-notification/internal/app/notification/delivery/pubsub"
)

// jhipster-needle-import-handler

type RequiredHandlers struct {
	HealthCheckHTTPHandlerRegistry    *healthcheckweb.HealthCheckHandlerRegistry
	NotificationPubsubHandlerRegistry *notificationPubsub.NotificationHandlerRegistry
}
