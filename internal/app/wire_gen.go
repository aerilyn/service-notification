// Code generated by Wire. DO NOT EDIT.

//go:generate go run github.com/google/wire/cmd/wire
//go:build !wireinject
// +build !wireinject

package app

import (
	"github.com/google/wire"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/cryptography"
	"gitlab.com/aerilyn/service-library/cryptography/aes256"
	"gitlab.com/aerilyn/service-library/hash/md5"
	"gitlab.com/aerilyn/service-library/httpRequest"
	"gitlab.com/aerilyn/service-library/httpRequest/usecase"
	"gitlab.com/aerilyn/service-library/jwt"
	"gitlab.com/aerilyn/service-library/middleware/auth"
	"gitlab.com/aerilyn/service-library/middleware/authorization"
	"gitlab.com/aerilyn/service-library/middleware/header"
	"gitlab.com/aerilyn/service-library/middleware/log"
	"gitlab.com/aerilyn/service-library/middleware/pubsub/watermill"
	"gitlab.com/aerilyn/service-library/module"
	"gitlab.com/aerilyn/service-library/otel/newrelic"
	"gitlab.com/aerilyn/service-library/pubsub"
	"gitlab.com/aerilyn/service-notification/internal/app/healthcheck/delivery/web"
	pubsub2 "gitlab.com/aerilyn/service-notification/internal/app/notification/delivery/pubsub"
	"gitlab.com/aerilyn/service-notification/internal/app/notification/usecase/usecaseimpl"
	"gitlab.com/aerilyn/service-notification/internal/pkg/cache"
	"gitlab.com/aerilyn/service-notification/internal/pkg/cache/redis"
	"gitlab.com/aerilyn/service-notification/internal/pkg/database/mongo"
	"gitlab.com/aerilyn/service-notification/internal/pkg/email/gmail"
	"gitlab.com/aerilyn/service-notification/internal/pkg/pubsub/rabbitmq"
	"gopkg.in/gomail.v2"
)

// Injectors from wire.go:

func InjectApp() (*Module, func(), error) {
	healthCheckHandlerRegistry := web.NewHandlerRegistry()
	configConfig := config.InjectConfig()
	config2 := config.ProvideConfig(configConfig)
	dialer := gmail.ProviderGmail(configConfig)
	emailSuccessRegisterOptions := usecaseimpl.EmailSuccessRegisterOptions{
		Config:      config2,
		GmailDialer: dialer,
	}
	emailSuccessRegister := usecaseimpl.NewEmailSuccessRegister(emailSuccessRegisterOptions)
	loggerAdapter := pubsub.ProvideLoggerWithStdLogger(config2)
	amqpConfig := pubsub.ProvideDurableQueueConfig(config2)
	subscriber := rabbitmq.NewProviderSubscriberWithAMQP(loggerAdapter, amqpConfig)
	notificationHandlerRegistryOptions := pubsub2.NotificationHandlerRegistryOptions{
		EmailSuccessRegister: emailSuccessRegister,
		Subscriber:           subscriber,
		Config:               configConfig,
	}
	notificationHandlerRegistry := pubsub2.NewNotificationHandlerRegistry(notificationHandlerRegistryOptions)
	requiredHandlers := RequiredHandlers{
		HealthCheckHTTPHandlerRegistry:    healthCheckHandlerRegistry,
		NotificationPubsubHandlerRegistry: notificationHandlerRegistry,
	}
	newRelic := newrelic.ProvideNewRelic(config2)
	newReliMiddlewareRegistry := newrelic.NewNewReLicMiddlewareRegistry(newRelic)
	headerMiddlewareOpts := header.HeaderMiddlewareOpts{
		Config: configConfig,
	}
	headerMiddlewareRegistry := header.NewHeaderMiddlewareRegistry(headerMiddlewareOpts)
	loggingMiddlewareRegistry := log.NewLoggingMiddlewareRegistry(configConfig)
	jwtOpts := jwt.ProvideJwt(config2)
	aes256Opts := aes256.Aes256Opts{
		Config: configConfig,
	}
	aes256Aes256 := aes256.ProvideAes256(aes256Opts)
	authMiddlewareOpts := auth.AuthMiddlewareOpts{
		Config: configConfig,
		Jwt:    jwtOpts,
		Crypto: aes256Aes256,
	}
	authMiddlewareRegistry := auth.NewAuthMiddlewareRegistry(authMiddlewareOpts)
	httpRequestHelper := usecase.NewHttpRequestHelper()
	authorizationMiddlewareRegistryOptions := authorization.AuthorizationMiddlewareRegistryOptions{
		HttpRequestHelper: httpRequestHelper,
		Config:            configConfig,
	}
	authorizationMiddlewareRegistry := authorization.NewAuthorizationMiddlewareRegistry(authorizationMiddlewareRegistryOptions)
	watermillStdMiddlewareRegistry := watermill.NewWatermillStdMiddlewareRegistry(loggerAdapter)
	requiredMiddlewares := RequiredMiddlewares{
		NewRelic:      newReliMiddlewareRegistry,
		Header:        headerMiddlewareRegistry,
		Logging:       loggingMiddlewareRegistry,
		Auth:          authMiddlewareRegistry,
		Authorization: authorizationMiddlewareRegistry,
		Watermill:     watermillStdMiddlewareRegistry,
	}
	router, err := pubsub.ProvideRouter(loggerAdapter)
	if err != nil {
		return nil, nil, err
	}
	runner := pubsub.NewRunner(router)
	moduleOptions := ModuleOptions{
		RequiredHandlers:    requiredHandlers,
		RequiredMiddlewares: requiredMiddlewares,
		Config:              config2,
		PubSubRunner:        runner,
	}
	module := NewModule(moduleOptions)
	return module, func() {
	}, nil
}

// wire.go:

var ModuleSet = wire.NewSet(wire.Struct(new(RequiredHandlers), "*"), wire.Struct(new(RequiredMiddlewares), "*"), wire.Struct(new(ModuleOptions), "*"), wire.Struct(new(aes256.Aes256Opts), "*"), wire.Struct(new(auth.AuthMiddlewareOpts), "*"), wire.Struct(new(authorization.AuthorizationMiddlewareRegistryOptions), "*"), wire.Struct(new(header.HeaderMiddlewareOpts), "*"), wire.Bind(new(cache.Cache), new(*redis.RedisClient)), wire.Bind(new(gmail.GomailDialer), new(*gomail.Dialer)), wire.Bind(new(jwt.Jwt), new(*jwt.JwtOpts)), wire.Bind(new(config.ConfigEnv), new(*config.Config)), wire.Bind(new(httpRequest.HttpRequestHelper), new(*usecase.HttpRequestHelper)), wire.Bind(new(cryptography.Cryptography), new(*aes256.Aes256)), wire.Bind(new(md5.HashMD5), new(*md5.HashMD5Impl)), NewModule, config.InjectConfig, config.ProvideConfig, mongo.ProvideClient, redis.ProvideClient, newrelic.ProvideNewRelic, log.NewLoggingMiddlewareRegistry, newrelic.NewNewReLicMiddlewareRegistry, auth.NewAuthMiddlewareRegistry, authorization.NewAuthorizationMiddlewareRegistry, header.NewHeaderMiddlewareRegistry, usecase.NewHttpRequestHelper, jwt.ProvideJwt, aes256.ProvideAes256, md5.NewHashMD5, module.NewApplicationDelegate, pubsub.NewRunner, pubsub.ProvideLoggerWithStdLogger, pubsub.ProvideDurableQueueConfig, pubsub.ProvideRouter, rabbitmq.NewProviderSubscriberWithAMQP, rabbitmq.NewProviderPublihserWithAMQP, gmail.ProviderGmail, watermill.NewWatermillStdMiddlewareRegistry)
